Lab-SCALE app catalog
=====================

A TrueNAS SCALE app catalog for small team integration with GitLab.

Any up-to-date [documentation][1] will be found in [the wiki][1].

---

### [GitLab Agent][2] 🕶️ <small>*deployment*</small>

### [GitLab Runner][3] 👟 <small>*builds*</small>

---

Use common on-premises resources to get your private dev team
or homelab up-to-speed on Kubernetes and GitOps.

Build your on-site computing and storage cluster running
[TrueNAS SCALE](https://www.truenas.com/truenas-scale/).

Attach that cluster to any hosted or self-hosted
[GitLab](https://about.gitlab.com/) project.

Move your development infrastructure in-house:
cap your bursty build and testing costs; flatten
your external production workload costs.

---

👋 Pull requests welcome; _I would be positively **thrilled**
   for GitLab or iXsystems to take this off of my hands…_

[1]: <https://gitlab.com/lab-scale/catalog/-/wikis/> "Documentation"
[2]: <https://gitlab.com/lab-scale/catalog/-/wikis/apps/GitLabAgent.md>
[3]: <https://gitlab.com/lab-scale/catalog/-/wikis/apps/GitLabRunner.md>
