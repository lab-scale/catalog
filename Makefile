AGENT_VERSION=0.9.0
RUNNER_VERSION=0.9.0

TRAIN=charts

#ORCHESTRATOR=docker
ORCHESTRATOR=podman

.PHONY: all
all: update lint-agent lint-runner catalog validate

.PHONY: update
update:
	python3 update_dependencies update --train $(TRAIN)

.PHONY: catalog
catalog:
	$(ORCHESTRATOR) run -it --rm \
		--platform linux/amd64 \
		-v "$(CURDIR)":/catalog \
		ixsystems/catalog_validation \
		catalog_update update --path /catalog

.PHONY: validate
validate:
	$(ORCHESTRATOR) run -it --rm \
		--platform linux/amd64 \
		-v "$(CURDIR)":/catalog \
		ixsystems/catalog_validation \
		catalog_validate validate --path /catalog

.PHONY: deploy
deploy:
	$(ORCHESTRATOR) run -it --rm \
		--platform linux/amd64 \
		-v "$(CURDIR)":/catalog \
		ixsystems/catalog_validation \
		charts_validate deploy --path /catalog --base_branch main

.PHONY: lint-agent
lint-agent:
	cd $(TRAIN)/gitlab-agent/$(AGENT_VERSION) && \
	helm lint -f ix_values.yaml --with-subcharts

.PHONY: lint-runner
lint-runner:
	cd $(TRAIN)/gitlab-runner/$(RUNNER_VERSION) && \
	helm lint -f ix_values.yaml --with-subcharts


.PHONY: agent
agent:
	helm template gitlab-agent \
		--skip-crds \
		./$(TRAIN)/gitlab-agent/$(AGENT_VERSION) \
		-f $(TRAIN)/gitlab-agent/$(AGENT_VERSION)/ix_values.yaml

.PHONY: runner
runner:
	helm template gitlab-runner \
		--skip-crds \
		./$(TRAIN)/gitlab-runner/$(RUNNER_VERSION) \
		-f $(TRAIN)/gitlab-runner/$(RUNNER_VERSION)/ix_values.yaml
